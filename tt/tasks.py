import enum


class TaskState(enum.Enum):
    stop = 1
    start = 2
    pause = 3


class Task(object):
    state = TaskState.stop

    def __repr__(self):
        return '<{}: {}>'.format(type(self).__name__, self)

    def start(self):
        raise NotImplementedError()

    def stop(self):
        raise NotImplementedError()

    def pause(self):
        raise NotImplementedError()

    def resume(self):
        raise NotImplementedError()


class NoopTask(Task):

    def __init__(self, name):
        super().__init__()
        self.name = name

    def __str__(self):
        return self.name

    @classmethod
    def none(cls):
        return cls("None!")

    @classmethod
    def slacking_off(cls):
        return cls("Slacking off")

    def start(self):
        self.state = TaskState.start

    def stop(self):
        self.state = TaskState.stop

    def pause(self):
        self.state = TaskState.pause

    def resume(self):
        self.start = TaskState.start
