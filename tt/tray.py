from gi.repository import Gtk

from .paydirt.tasks import new_paydirt_task, get_current_task
from .tasks import NoopTask


class TTStatusIcon(Gtk.StatusIcon):

    def __init__(self, manager):
        super().__init__()

        manager.connect("change-task", self.on_change_task)

        self.manager = manager
        self.set_from_icon_name("view-refresh")
        self.set_tooltip_text("Time tracker")

        self.status_menu = StatusMenu(manager)

        self.connect("activate", lambda _: manager.annoy())
        self.connect("popup-menu", self.on_popup_menu)

    def on_change_task(self, manager, old_task, new_task):
        self.set_tooltip_text("Time tracker\nCurrent task: {}".format(new_task))

        if isinstance(new_task, NoopTask):
            self.set_from_icon_name("dialog-error")
        else:
            self.set_from_icon_name("appointment-soon")

    def on_clicked(self, _):
        Gtk.main_quit()

    def on_popup_menu(self, _, button, activate_time):
        self.status_menu.popup(None, None, None, None, button, activate_time)


class StatusMenu(Gtk.Menu):
    def __init__(self, manager):
        super().__init__()
        self.manager = manager

        set_task = Gtk.MenuItem(label="Set task")
        set_task.connect("activate", lambda _: new_paydirt_task(
            self.manager, self.manager.new_task))
        self.add(set_task)

        stop = Gtk.MenuItem(label="Stop for the day")
        stop.connect("activate", lambda _: manager.sleep())
        self.add(stop)

        snooze = Gtk.MenuItem(label="Slack off")
        snooze.connect("activate", lambda _: manager.snooze())
        self.add(snooze)

        self.add(Gtk.SeparatorMenuItem())

        get_task = Gtk.MenuItem(label="Get curent task from Paydirt")
        get_task.connect("activate", lambda _: get_current_task(
            self.manager, self.manager.new_task))
        self.add(get_task)

        self.add(Gtk.SeparatorMenuItem())

        quit = Gtk.MenuItem(label="Quit")
        quit.connect("activate", Gtk.main_quit)
        self.add(quit)
        self.show_all()
