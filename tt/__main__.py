import datetime
import dbus
import dbus.mainloop.glib

# from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import Gtk

from tt.manager import Manager
from tt.sleepy import Sleeper
from tt.tray import TTStatusIcon
from tt.paydirt.tasks import get_current_task


class Settings(object):
    snooze_length = datetime.timedelta(minutes=6)
    wake_time = datetime.time(hour=8)

    paydirt_email = ""
    paydirt_password = ""


def main():
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    sleeper = Sleeper()
    manager = Manager(sleeper, Settings)
    status_icon = TTStatusIcon(manager)
    status_icon.set_visible(True)

    def got_current_task(task):
        manager.current_task = task
        manager.annoy()

    get_current_task(manager, got_current_task)

    manager.connect("change-task", log_signal("change-task"))

    Gtk.main()


def log_signal(signal):
    def logger(source, *args):
        print('{} {}: {}'.format(source, signal, ', '.join(map(repr, args))))
    return logger


if __name__ == '__main__':
    main()
