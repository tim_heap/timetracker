import datetime
import notify2

from gi.repository import GObject
from .annoying import Annoyer
from .tasks import NoopTask
from .utils import in_gtk, CancellableCallback


class Manager(GObject.GObject):

    __gsignals__ = {
        "sleep": (GObject.SignalFlags.ACTION, None, []),
        "snooze": (GObject.SignalFlags.ACTION, None, []),
        "resume": (GObject.SignalFlags.ACTION, None, []),
        "change-task": (GObject.SignalFlags.ACTION, None,
                        [object, object]),
    }

    settings = None
    annoyer = None

    def __init__(self, sleeper, settings):
        super().__init__()
        self.sleeper = sleeper
        self.settings = settings

        notify2.init("tt")

        self.current_task = NoopTask.none()

    _current_task = None

    def _get_current_task(self):
        return self._current_task

    def _set_current_task(self, new_task):
        old_task = self._current_task

        self._current_task = new_task
        self.emit("change-task", old_task, new_task)

    current_task = property(_get_current_task, _set_current_task)

    current_callback = None

    def callback(self, fn):
        if self.current_callback:
            self.current_callback.cancel()
        self.current_callback = CancellableCallback(fn)
        return self.current_callback

    def schedule_reminder_then_annoy(self):
        def notify():
            self.show_reminder()
            self.sleeper.sleep_for(datetime.timedelta(minutes=1),
                                   self.callback(self.annoy))

        self.sleeper.sleep_for(self.settings.snooze_length,
                               self.callback(notify))

    def show_reminder(self):
        message = "Are you still working on {}?".format(self.current_task)
        notification = notify2.Notification("Time tracker", message)

        def on_yep(notification, action, *args):
            self.resume_task()

        def on_nope(notification, action, *args):
            self.annoy()

        notification.set_timeout(60 * 1000)
        notification.add_action("yep", "Yep!", on_yep, None)
        notification.add_action("nope", "Nope!", on_nope, None)
        notification.show()

    @in_gtk
    def annoy(self):
        if self.current_callback:
            self.current_callback.cancel()
        annoyer = Annoyer(self)
        annoyer.show_all()

    def resume_task(self):
        self.current_task.resume()
        self.schedule_reminder_then_annoy()

    def new_task(self, task):
        self.current_task = task
        self.schedule_reminder_then_annoy()

    def sleep(self):
        self.current_task.stop()
        self.current_task = NoopTask.none()
        self.sleeper.sleep_till_time(self.settings.wake_time,
                                     self.callback(self.annoy))

    def snooze(self):
        self.current_task.stop()
        self.current_task = NoopTask.slacking_off()
        self.schedule_reminder_then_annoy()

    def __repr__(self):
        return '<Manager>'
