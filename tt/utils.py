import threading

from functools import wraps
from gi.repository import GLib, Gtk, Gdk


def in_gtk(fn):
    """
    Queue a function up to be executed in the GTK main loop.
    """
    @wraps(fn)
    def decorated(*args, **kwargs):
        GLib.idle_add(fn, *args, **kwargs)
    return decorated


@in_gtk
def ask_question(question, callback):
    win = Gtk.Window(title="Question")
    win.set_border_width(10)
    win.set_type_hint(Gdk.WindowTypeHint.DIALOG)

    vbox = Gtk.Box(spacing=20, orientation=Gtk.Orientation.VERTICAL,
                   halign=Gtk.Align.START)
    win.add(vbox)

    top = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, halign=Gtk.Align.START)
    vbox.add(top)

    top.add(Gtk.Label(halign=Gtk.Align.START, label=question))

    input = Gtk.Entry()
    top.add(input)

    button_row = Gtk.Box(spacing=6, halign=Gtk.Align.END)
    vbox.add(button_row)

    def on_done(widget):
        name = input.get_text()
        win.close()
        callback(name)

    done = Gtk.Button("Done")
    done.connect("clicked", on_done)
    button_row.pack_end(done, True, True, 0)

    def on_cancel(widget):
        win.close()
        callback(None)

    cancel = Gtk.Button("Cancel")
    cancel.connect("clicked", on_cancel)
    button_row.pack_end(cancel, True, True, 0)

    win.show_all()


class Callback(object):

    def __init__(self, fn):
        self.fn = fn

    def __call__(self, *args, **kwargs):
        return self.fn(*args, **kwargs)


class CancellableCallback(Callback):
    cancelled = False

    def __call__(self, *args, **kwargs):
        if not self.cancelled:
            return super().__call__(*args, **kwargs)

    def cancel(self):
        self.cancelled = True

    def __repr__(self):
        return '<{}: {} {!r}>'.format(type(self).__name__, id(self), self.fn)


class CallbackOnce(Callback):
    called = False

    def __call__(self, *args, **kwargs):
        if not self.called:
            return super().__call__(*args, **kwargs)
        self.called = True


def new_thread(fn=None, **thread_options):
    if fn is None:
        return lambda fn: new_thread(fn, **thread_options)

    @wraps(fn)
    def new_fn(*args, **kwargs):
        thread = threading.Thread(target=fn, args=args, kwargs=kwargs,
                                  **thread_options)
        thread.start()
    return new_fn
