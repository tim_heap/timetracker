import datetime
import enum

from gi.repository import Gtk, Gdk

from .paydirt.tasks import new_paydirt_task


class Response(enum.IntEnum):
    sleep = 1
    snooze = 2
    new = 3
    resume = 3


class Annoyer(Gtk.Window):

    def __init__(self, manager):
        super().__init__(title="Time tracking")

        self.set_border_width(10)
        self.set_type_hint(Gdk.WindowTypeHint.DIALOG)
        self.set_urgency_hint(True)
        self.stick()

        self.manager = manager

        vbox = Gtk.Box(spacing=20, orientation=Gtk.Orientation.VERTICAL,
                       halign=Gtk.Align.START)
        self.add(vbox)

        label = Gtk.Label(halign=Gtk.Align.START)
        template = "\n\n".join(["Current task: <b>{}</b>",
                                "Time: <b>{}</b>"])
        label.set_markup(template.format(
            self.manager.current_task,
            datetime.datetime.now().strftime("%Y/%m/%d %H:%M")))
        vbox.pack_start(label, True, False, 0)

        hbox = Gtk.Box(spacing=6, halign=Gtk.Align.END)
        vbox.pack_start(hbox, False, True, 0)

        self.b_continue = Gtk.Button(label="Continue")
        self.b_continue.connect("clicked", self.on_resume)
        self.b_new_task = Gtk.Button(label="Start a new task")
        self.b_new_task.connect("clicked", self.on_new_task)
        self.b_snooze = Gtk.Button(label="Slack off")
        self.b_snooze.connect("clicked", self.on_snooze)
        self.b_sleep = Gtk.Button(label="Stop for the day")
        self.b_sleep.connect("clicked", self.on_sleep)

        hbox.pack_end(self.b_continue, True, True, 0)
        hbox.pack_end(self.b_new_task, True, True, 0)
        hbox.pack_end(self.b_snooze, True, True, 0)
        hbox.pack_end(self.b_sleep, True, True, 0)

        self.connect("delete-event", self.on_close)

    def on_sleep(self, _):
        self.manager.sleep()
        self.destroy()

    def on_snooze(self, _):
        self.manager.snooze()
        self.destroy()

    def on_new_task(self, _):
        new_paydirt_task(self.manager, self.manager.new_task)
        self.destroy()

    def on_resume(self, _):
        self.manager.resume_task()
        self.destroy()

    def on_close(self, _a, _b):
        self.manager.snooze()
        self.destroy()
