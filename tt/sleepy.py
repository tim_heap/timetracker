import datetime
import time

from .utils import new_thread


class Sleeper(object):

    def sleep_for(self, timedelta, callback):
        self.sleep_till_datetime(datetime.datetime.now() + timedelta, callback)

    def sleep_till_time(self, wake_time, callback):
        now = datetime.datetime.now()
        current_time = now.time()
        current_date = now.date()

        if current_time > wake_time:
            wake_date = current_date + datetime.timedelta(days=1)
        else:
            wake_date = current_date

        return self.sleep_till_datetime(datetime.datetime.combine(
            wake_date, wake_time), callback)

    @new_thread(daemon=True)
    def sleep_till_datetime(self, when, callback):
        sleep_till_datetime(when)
        callback()


def sleep_till_datetime(when, max_sleep=datetime.timedelta(minutes=1)):
    """
    Sleep till a given datetime, and call `done` when completed. Sleeping is
    done in chunks of `max_sleep`, which should be reasonably small. This way,
    if the computer is suspended, or the system time changed, etc, the wake up
    time will be overshot by at most `max_sleep`.
    """

    while True:
        now = datetime.datetime.now()
        diff = when - now
        if now > when:
            break
        elif diff > max_sleep:
            time.sleep(max_sleep.total_seconds())
        else:
            time.sleep(diff.total_seconds())
