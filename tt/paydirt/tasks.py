import subprocess

from ..tasks import Task, TaskState, NoopTask
from ..utils import new_thread

from . import api

current_timer_url = 'https://paydirtapp.com/tracker'


class PaydirtTask(Task):

    def __init__(self, manager, running_block):
        super().__init__()
        self.running_block = running_block
        self.manager = manager

    def __str__(self):
        return '{} » {} » {}'.format(
            self.running_block['client_business_name'],
            self.running_block['project_name'],
            self.running_block['job_name'])

    def start(self):
        self.state = TaskState.start

    @new_thread
    def stop(self):
        @new_thread
        def stop():
            session = api.get_logged_in_session(self.manager)
            api.stop_current_timer(session)

        stop()
        self.state = TaskState.stop

    def pause(self):
        start_browser(current_timer_url)
        self.state = TaskState.pause

    def resume(self):
        self.start = TaskState.start


@new_thread(daemon=True)
def get_current_task(manager, callback):
    """
    When the API supports getting the current task, this can be done in a
    better way...
    """
    session = api.get_logged_in_session(manager)
    running_block = api.get_running_block(session)
    if running_block:
        callback(PaydirtTask(manager, running_block))
    else:
        callback(NoopTask.none())


@new_thread(daemon=True)
def new_paydirt_task(manager, callback):
    """
    Launch a browser to prompt the user to start a new time log,
    then call the callback once done.
    A new thread is started to monitor the browser and call the callback
    """
    start_browser(current_timer_url)
    get_current_task(manager, callback)


def start_browser(url):
    """
    Launch a browser to start a new timer.
    """
    subprocess.call(['firefox', '--new-instance', '-P', 'paydirt',
                     '--new-window', url])
