import bs4
import requests

paydirt_base = 'https://paydirtapp.com/'
login_url = paydirt_base + 'login'
data_url = paydirt_base + 'data/all'

stop_url = paydirt_base + 'tracker/stop'


def login(session, email, password):
    res = session.get(login_url)
    soup = bs4.BeautifulSoup(res.text)
    form = soup.find('form', id='new_user_session')
    defaults = {input['name']: input.get('value')
                for input in form.find_all('input')}
    login = {'user_session[email]': email,
             'user_session[password]': password}
    csrf_token = defaults['authenticity_token']

    data = {}
    data.update(defaults)
    data.update(login)
    res = session.post(login_url, data=data, allow_redirects=False)
    if res.status_code != 302:
        raise RuntimeError("Failed to log in!")

    res = session.get(res.headers['Location'])
    doc = bs4.BeautifulSoup(res.text)
    csrf_token = doc.find('meta', attrs={'name': 'csrf-token'})
    session.headers['X-Csrf-Token'] = csrf_token['content']


def get_logged_in_session(manager):
    session = requests.Session()
    settings = manager.settings
    login(session, settings.paydirt_email, settings.paydirt_password)
    return session


def get_all(session):
    return session.get(data_url).json()


def get_running_block(session):
    res = get_all(session)
    return res['data']['running_block']


def stop_current_timer(session, description=""):
    session.put(stop_url, {'description': description})
